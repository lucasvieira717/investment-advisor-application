<h1 align="center" id="title">Investment Advisor Application</h1>
<br>
<p align="center"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Inter_logo_2020.svg/1200px-Inter_logo_2020.svg.png" alt="project-image" width="200px"></p>
<br>
<p id="description">Lucas é um cliente do Inter e gostaria de começar a investir na bolsa via a plataforma de investimentos do Inter. Ele sabe que uma boa ideia é diversificar os investimentos ou seja não comprar ações somente de uma empresa mas sim de várias. Porém ele não sabe por onde começar e quais empresas ele deve comprar. O Inter ficou sabendo do problema do Lucas e designou você para implementar uma solução bem legal para que ele e outros milhões de clientes do Inter invistam sem problemas</p>

<h2>🛠️ Passos para executar a aplicação:</h2>

<p>1. Verificar Dependências</p>

Solicite ao Maven para baixar as dependências do projeto antes de executá-lo

<p>2. Acesso ao Banco</p>

O banco pode ser acessado pelo link da aplicação adicionando h2 no final. Ex: http://localhost:8080/h2

<p>3. Acesso a Documentação</p>

Para acessar a documentação acesse o link da aplicação colocando ao final api-docs. Ex: http://localhost:8080/api-docs

<h2>📋Observações do Projeto</h2>

<p>O banco ao executar salva seus arquivos no endereço: C:/data/investmentadvisordb</p>

<p>
    <strong>Caso seu sistema operacional não seja Windows altere o endereço no arquivo: 'application.properties'</strong>
</p>

<p> Ainda estou aprendendo sobre testes unitários, pois na outra empresa que trabalhei não era uma prática muito utilizada, tentei aplicar no projeto, porém o código ficou quebrado, por isto removi. Porém já comprei um curso que aborda o teste e já estou estudando o mesmo com o Spring Boot.</p>

<p> A parte documental acredito ter faltado mais detalhamento, apliquei o que aprendi esta semana, pois a documentação de API foi meu primeiro contato também</p>

<h2>💻 Tecnologias utilizadas</h2>

Tecnologias utilizadas no projeto:

*   Maven
*   JDK11
*   Spring Boot 2.6.4
*   Open API 1.6.6
*   H2 Database
