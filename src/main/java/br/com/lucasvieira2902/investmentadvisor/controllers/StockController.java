package br.com.lucasvieira2902.investmentadvisor.controllers;

import br.com.lucasvieira2902.investmentadvisor.dtos.StockPriceUpdateDto;
import br.com.lucasvieira2902.investmentadvisor.dtos.StockStoreDto;
import br.com.lucasvieira2902.investmentadvisor.entities.Company;
import br.com.lucasvieira2902.investmentadvisor.entities.Stock;
import br.com.lucasvieira2902.investmentadvisor.services.ICompanyService;
import br.com.lucasvieira2902.investmentadvisor.services.IStockService;
import io.swagger.v3.oas.annotations.Operation;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/stocks")
public class StockController {

    private final IStockService stockService;
    private final ICompanyService companyService;

    public StockController(IStockService stockService, ICompanyService companyService) {
        this.stockService = stockService;
        this.companyService = companyService;
    }

    @GetMapping
    @Operation(
            summary = "Return all stocks",
            description = "Return all stocks registered in the system",
            tags = {"Stocks"}
    )
    public ResponseEntity<Page<Stock>> getAllStocks(@ParameterObject Pageable pageable) {
        Page<Stock> stocks = stockService.findAll(pageable);
        if (stocks.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(stocks);
        }else{
            for (Stock stock : stocks) {
                UUID id = stock.getId();
                stock.add(linkTo(methodOn(StockController.class).getOneStock(id)).withSelfRel());
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(stocks);
    }

    @GetMapping("/{id}")
    @Operation(
            summary = "Return one stock",
            description = "Return one stock registered in the system",
            tags = {"Stocks"}
    )
    public ResponseEntity<Object> getOneStock(@PathVariable(value = "id") UUID id) {
        Optional<Stock> stockOptional = stockService.findById(id);
        if(!stockOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Stock not found");
        }
        stockOptional.get().setCompany(companyService.findById(stockOptional.get().getCompany().getId()).get());
        stockOptional.get().add(linkTo(methodOn(StockController.class).getOneStock(id)).withSelfRel());
        return ResponseEntity.status(HttpStatus.OK).body(stockOptional.get());
    }

    @PostMapping
    @Operation(
            summary = "Create a new stock",
            description = "Create a new stock in the system",
            tags = {"Stocks"}
    )
    public ResponseEntity<Object> saveStock(@RequestBody @Valid StockStoreDto stockStoreDto) {
        if(stockService.existsByTicker(stockStoreDto.getTicker())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Conflict: Stock already exists");
        }
        var stock = new Stock();
        BeanUtils.copyProperties(stockStoreDto, stock);
        stock.setCompany(companyService.findByNameAndIndStatusIsTrue(stockStoreDto.getCompany().getName()));
        if(stock.getCompany() == null) {
            var company = new Company();
            BeanUtils.copyProperties(stockStoreDto.getCompany(), company);
            company.setIndStatus(true);
            stock.setCompany(companyService.save(company));
        }
        return ResponseEntity
                .status(HttpStatus.CREATED).body(stockService.save(stock)
                .add(linkTo(methodOn(StockController.class).getOneStock(stock.getId())).withSelfRel()));
    }

    @DeleteMapping("/{id}")
    @Operation(
            summary = "Delete a stock",
            description = "Delete a stock registered in the system",
            tags = {"Stocks"}
    )
    public ResponseEntity<Object> deleteStock(@PathVariable(value = "id") UUID id) {
        Optional<Stock> stockOptional = stockService.findById(id);
        if(!stockOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Stock not found");
        }
        stockService.delete(stockOptional.get());
        return ResponseEntity.status(HttpStatus.OK).body("Stock deleted");
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Update a stock",
            description = "Update a stock registered in the system",
            tags = {"Stocks"}
    )
    public ResponseEntity<Object> updateStock(@PathVariable(value = "id") UUID id, @RequestBody @Valid StockPriceUpdateDto stockPriceUpdateDto) {
        Optional<Stock> stockOptional = stockService.findById(id);
        if(!stockOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Stock not found");
        }
        var stock = stockOptional.get();
        stock.setPrice(stockPriceUpdateDto.getPrice());
        return ResponseEntity
                .status(HttpStatus.OK).body(stockService.save(stock)
                .add(linkTo(methodOn(StockController.class).getOneStock(stock.getId())).withSelfRel()));
    }
}
