package br.com.lucasvieira2902.investmentadvisor.controllers;

import br.com.lucasvieira2902.investmentadvisor.config.validator.ErrorHandler;
import br.com.lucasvieira2902.investmentadvisor.dtos.CompanyIndStatusUpdateDto;
import br.com.lucasvieira2902.investmentadvisor.dtos.CompanyStoreDto;
import br.com.lucasvieira2902.investmentadvisor.entities.Company;
import br.com.lucasvieira2902.investmentadvisor.services.ICompanyService;
import br.com.lucasvieira2902.investmentadvisor.services.IStockService;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.links.Link;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/companies")
//@Api(value = "Empresas", description = "Empresas")
public class CompanyController {

    private final ICompanyService companyService;
    private final IStockService stockService;

    public CompanyController(ICompanyService companyService, IStockService stockService) {
        this.companyService = companyService;
        this.stockService = stockService;
    }

    @GetMapping
    @Operation(
            summary = "Return all companies",
            description = "Returns all companies registered in the system and their respective active and/or inactive",
            tags = {"Companies"}
    )
    public ResponseEntity<Page<Company>> getAllCompanies(@ParameterObject Pageable pageable, Optional<Boolean> indStatus) {
        Page<Company> companies;
        if(indStatus.isPresent()) {
            companies = companyService.findAllByIndStatus(pageable, indStatus.get());
        }else {
            companies = companyService.findAll(pageable);
        }
        if (companies.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }else{
            for (Company company : companies) {
                UUID id = company.getId();
                company.add(linkTo(methodOn(CompanyController.class).getOneCompany(id)).withSelfRel());
                company.getStocks().forEach(stock -> {
                    stock.add(linkTo(methodOn(StockController.class).getOneStock(stock.getId())).withSelfRel());
                });
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(companies);
    }

    @GetMapping("/{id}")
    @Operation(
            summary = "Return one company",
            description = "Returns one company registered in the system",
            tags = {"Companies"}
    )
    public ResponseEntity<Object> getOneCompany(@PathVariable(value = "id") UUID id) {
        Optional<Company> companyOptional = companyService.findById(id);

        if(!companyOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Company not found");
        }

        companyOptional.get().getStocks().forEach(stock -> {
            stock.add(linkTo(methodOn(StockController.class).getOneStock(stock.getId())).withSelfRel());
        });

        return ResponseEntity.status(HttpStatus.OK).body(companyOptional.get()
                .add(linkTo(methodOn(CompanyController.class).getOneCompany(id)).withSelfRel()));
    }

    @PostMapping
    @Operation(
            summary = "Create a new company",
            description = "Creates a new company in the system",
            tags = {"Companies"}
    )
    public ResponseEntity<Object> saveCompany(@RequestBody @Valid CompanyStoreDto companyStoreDto) {
        if(companyService.existsByName(companyStoreDto.getName())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Conflict: Company already exists");
        }
        var company = new Company();
        BeanUtils.copyProperties(companyStoreDto, company);
        company.setIndStatus(true);
        return ResponseEntity
                .status(HttpStatus.CREATED).body(companyService.save(company)
                .add(linkTo(methodOn(CompanyController.class).getOneCompany(company.getId())).withSelfRel()));
    }

    @DeleteMapping("/{id}")
    @Operation(
            summary = "Delete a company",
            description = "Delete a company registered in the system",
            tags = {"Companies"}
    )
    public ResponseEntity<Object> deleteCompany(@PathVariable(value = "id") UUID id) {
        Optional<Company> companyOptional = companyService.findById(id);
        if(!companyOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Company not found");
        }
        //caso tenha vinculo com algum stock, não pode ser deletado
        if(companyOptional.get().getStocks().size() > 0) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Conflict: Not is possible delete a company with stocks");
        }
        companyService.delete(companyOptional.get());
        return ResponseEntity.status(HttpStatus.OK).body("Company deleted");
    }

    @PutMapping("/{id}")
    @Operation(
            summary = "Update a status of a company",
            description = "Update a status of a company registered in the system",
            tags = {"Companies"}
    )
    public ResponseEntity<Object> updateCompany(@PathVariable(value = "id") UUID id, @RequestBody @Valid CompanyIndStatusUpdateDto companyIndStatusUpdateDto) {
        Optional<Company> companyOptional = companyService.findById(id);
        if(!companyOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Company not found");
        }
        var company = companyOptional.get();
        BeanUtils.copyProperties(companyIndStatusUpdateDto, company);
        return ResponseEntity.status(HttpStatus.OK).body(companyService.save(company)
                .add(linkTo(methodOn(CompanyController.class).getOneCompany(company.getId())).withSelfRel()));
    }
}
