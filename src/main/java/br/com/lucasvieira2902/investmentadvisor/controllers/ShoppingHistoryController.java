package br.com.lucasvieira2902.investmentadvisor.controllers;

import br.com.lucasvieira2902.investmentadvisor.dtos.ShoppingHistoryBuyDto;
import br.com.lucasvieira2902.investmentadvisor.dtos.ShoppingHistorySimulateDto;
import br.com.lucasvieira2902.investmentadvisor.entities.Company;
import br.com.lucasvieira2902.investmentadvisor.entities.ShoppingHistory;
import br.com.lucasvieira2902.investmentadvisor.entities.ShoppingHistoryItem;
import br.com.lucasvieira2902.investmentadvisor.services.IShoppingHistoryService;
import io.swagger.v3.oas.annotations.Operation;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.reactive.WebFluxLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/shopping")
public class ShoppingHistoryController {

    private final IShoppingHistoryService shoppingHistoryService;

    public ShoppingHistoryController(IShoppingHistoryService shoppingHistoryService) {
        this.shoppingHistoryService = shoppingHistoryService;
    }

    @PostMapping("/simulate")
    @Operation(
            summary = "Run Investment Simulation",
            description = "Returns the purchase of shares according to the amount to be invested and " +
                    "the number of companies that the amount will be distributed among.",
            tags = "Investment"
    )
    public ResponseEntity<Object> simulateInvestment(@RequestBody @Valid ShoppingHistorySimulateDto shoppingHistorySimulateDto){
        return ResponseEntity.status(HttpStatus.CREATED).body(shoppingHistoryService.simulate(shoppingHistorySimulateDto));
    }

    @PostMapping()
    @Operation(
            summary = "Buy Investments",
            description = "Buy investments",
            tags = "Investment"
    )
    public ResponseEntity<Object> saveShoppingHistory(@RequestBody @Valid ShoppingHistoryBuyDto shoppingHistoryBuyDto) throws Exception {
        try{
            return ResponseEntity.status(HttpStatus.CREATED).body(shoppingHistoryService.save(shoppingHistoryBuyDto));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    @Operation(
            summary = "Get Investment",
            description = "Get investment",
            tags = "Investment"
    )
    public ResponseEntity<Object> getInvestment(@PathVariable(value = "id") UUID id) {
        Optional<ShoppingHistory> shoppingHistoryOptional = shoppingHistoryService.findById(id);
        if(!shoppingHistoryOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Stock not found");
        }
        shoppingHistoryOptional.get().add(linkTo(methodOn(ShoppingHistoryController.class).getInvestment(id)).withSelfRel());
        return ResponseEntity.status(HttpStatus.OK).body(shoppingHistoryOptional.get());
    }

    @GetMapping
    @Operation(
            summary = "Get Investments",
            description = "Get investments",
            tags = "Investment"
    )
    public ResponseEntity<Page<ShoppingHistory>> getInvestments(@ParameterObject Pageable pageable) {
        Page<ShoppingHistory> shopppingHistory = shoppingHistoryService.findAllShoppingHistory(pageable);
        if (shopppingHistory.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }else{
            for (ShoppingHistory shopppingHistoryTemp : shopppingHistory) {
                UUID id = shopppingHistoryTemp.getId();
                shopppingHistoryTemp.add(linkTo(methodOn(ShoppingHistoryController.class).getInvestment(id)).withSelfRel());
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(shopppingHistory);
    }

}
