package br.com.lucasvieira2902.investmentadvisor.dtos;

import java.util.List;

public class ShoppingHistoryBuyDto {

    private String cpf;
    private double totalInvestedValue;
    private List<ShoppingHistoryItemBuyDto> items;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public double getTotalInvestedValue() {
        return totalInvestedValue;
    }

    public void setTotalInvestedValue(double totalInvestedValue) {
        this.totalInvestedValue = totalInvestedValue;
    }

    public List<ShoppingHistoryItemBuyDto> getItems() {
        return items;
    }

    public void setItems(List<ShoppingHistoryItemBuyDto> items) {
        this.items = items;
    }
}
