package br.com.lucasvieira2902.investmentadvisor.dtos;


import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

public class StockStoreDto {

    @Schema(description = "Ticker of the stock", required = true, example = "BIDI11")
    @NotBlank
    private String ticker;

    @Schema(description = "Price of the stock", required = true, example = "10.0")
    @DecimalMin("0.0")
    @Positive
    private double price;

    @Schema(description = "Quantity of the stocks available", required = true, example = "55")
    @Min(1)
    private int quantity;

    @Schema(description = "Company", required = true, example = "{\"name\": \"Inter\"}")
    private CompanyStoreDto company;

    public StockStoreDto() {
    }

    public StockStoreDto(String ticker, double price, int quantity, CompanyStoreDto company) {
        this.ticker = ticker;
        this.price = price;
        this.quantity = quantity;
        this.company = company;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public CompanyStoreDto getCompany() {
        return company;
    }

    public void setCompany(CompanyStoreDto company) {
        this.company = company;
    }
}
