package br.com.lucasvieira2902.investmentadvisor.dtos;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import java.util.List;

public class CompanyIndStatusUpdateDto {

    @Schema(description = "Status of the company", example = "true")
    private Boolean indStatus;

    public CompanyIndStatusUpdateDto() {
    }

    public CompanyIndStatusUpdateDto(Boolean indStatus) {
        this.indStatus = indStatus;
    }

    public Boolean getIndStatus() {
        return indStatus;
    }

    public void setIndStatus(Boolean indStatus) {
        this.indStatus = indStatus;
    }
}
