package br.com.lucasvieira2902.investmentadvisor.dtos;


import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

public class StockPriceUpdateDto {

    @Schema(description = "Price of the stock", required = true, example = "10.0")
    @DecimalMin("0.0")
    @Positive
    private double price;

    public StockPriceUpdateDto() {
    }

    public StockPriceUpdateDto(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
