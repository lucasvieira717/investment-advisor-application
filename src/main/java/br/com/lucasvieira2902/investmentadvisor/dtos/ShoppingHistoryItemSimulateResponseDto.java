package br.com.lucasvieira2902.investmentadvisor.dtos;

import br.com.lucasvieira2902.investmentadvisor.entities.Stock;

public class ShoppingHistoryItemSimulateResponseDto {

    private Stock stock;
    private int quantity;
    private double price;

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
