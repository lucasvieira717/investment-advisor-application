package br.com.lucasvieira2902.investmentadvisor.dtos;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class ShoppingHistorySimulateDto {
    @NotBlank
    @Schema(description = "Number of CPF documents to simulate", example = "12345678901")
    private String cpf;

    @Min(value = 1)
    @Schema(description = "Number of different companies to simulate", example = "3")
    private int qtdCompaniesInvestment;

    @DecimalMin(value = "1.00")
    @Schema(description = "Investment value", example = "100.00")
    private Double totalInvestment;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getQtdCompaniesInvestment() {
        return qtdCompaniesInvestment;
    }

    public void setQtdCompaniesInvestment(int qtdCompaniesInvestment) {
        this.qtdCompaniesInvestment = qtdCompaniesInvestment;
    }

    public double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }
}
