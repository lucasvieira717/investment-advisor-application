package br.com.lucasvieira2902.investmentadvisor.dtos;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

public class ShoppingHistorySimulateResponseDto {

    private String cpf;
    private Double totalInvestedValue;
    private Double cashChange;
    private List<ShoppingHistoryItemSimulateResponseDto> items;

    public ShoppingHistorySimulateResponseDto() {
        this.items = new ArrayList<>();
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Double getTotalInvestedValue() {
        return totalInvestedValue;
    }

    public void setTotalInvestedValue(Double totalInvestedValue) {
        this.totalInvestedValue = totalInvestedValue;
    }

    public Double getCashChange() {
        return cashChange;
    }

    public void setCashChange(Double cashChange) {
        this.cashChange = cashChange;
    }

    public List<ShoppingHistoryItemSimulateResponseDto> getItems() {
        return items;
    }

    public void setItems(List<ShoppingHistoryItemSimulateResponseDto> items) {
        this.items = items;
    }
}
