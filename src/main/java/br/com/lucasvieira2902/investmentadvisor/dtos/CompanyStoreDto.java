package br.com.lucasvieira2902.investmentadvisor.dtos;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import java.util.List;

public class CompanyStoreDto {


    @Schema(description = "Name of the company", example = "Inter")
    @NotBlank
    private String name;

    @Schema(description = "Status of the company", hidden = true)
    private Boolean indStatus;

    @Schema(description = "List of the company's stocks", hidden = true)
    private List<StockStoreDto> stocks;

    public CompanyStoreDto() {
    }

    public CompanyStoreDto(String name, List<StockStoreDto> stocks, Boolean indStatus) {
        this.name = name;
        this.stocks = stocks;
        this.indStatus = indStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StockStoreDto> getStocks() {
        return stocks;
    }

    public void setStocks(List<StockStoreDto> stocks) {
        this.stocks = stocks;
    }

    public Boolean getIndStatus() {
        return indStatus;
    }

    public void setIndStatus(Boolean indStatus) {
        this.indStatus = indStatus;
    }
}
