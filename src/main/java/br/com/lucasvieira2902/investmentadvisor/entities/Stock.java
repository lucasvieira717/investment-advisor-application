package br.com.lucasvieira2902.investmentadvisor.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "stocks")
public class Stock extends RepresentationModel<Stock> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "ticker", nullable = false)
    private String ticker;

    @Column(name = "price", nullable = false)
    private double price;

    @Column(name = "quantity", nullable = false, columnDefinition = "int default 1")
    private int quantity;

    @ManyToOne()
    @JoinColumn(name = "company_id", nullable = false)
    private Company company;

    @OneToMany(mappedBy = "stock", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ShoppingHistoryItem> shoppingHistoryItem = new ArrayList<ShoppingHistoryItem>();

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public Stock() {}

    public Stock(String ticker, double price, int quantity, Company company, List<ShoppingHistoryItem> shoppingHistoryItem) {
        this.ticker = ticker;
        this.price = price;
        this.quantity = quantity;
        this.company = company;
        this.shoppingHistoryItem = shoppingHistoryItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stock)) return false;
        if (!super.equals(o)) return false;
        Stock stock = (Stock) o;
        return getId().equals(stock.getId());
    }

    @Override
    public String toString() {
        String objeto = "Stock{" +
                "id=" + id +
                ", ticker='" + ticker + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", company=" + company +
                ", shoppingHistoryItem=" + shoppingHistoryItem +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
        System.out.println(objeto);
        return objeto;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getId());
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @JsonBackReference
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @JsonManagedReference
    public List<ShoppingHistoryItem> getShoppingHistoryItem() {
        return shoppingHistoryItem;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

}
