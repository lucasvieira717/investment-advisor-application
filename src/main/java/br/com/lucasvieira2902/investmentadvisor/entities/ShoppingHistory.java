package br.com.lucasvieira2902.investmentadvisor.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "shopping_histories")
public class ShoppingHistory extends RepresentationModel<ShoppingHistory> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "cpf", nullable = false)
    private String cpf;

    @Column(name = "total_invested_value", nullable = false)
    private Double totalInvestedValue;

    @Column(name = "cash_change", nullable = false, columnDefinition = "DECIMAL(10,2)", precision = 10, scale = 2)
    private double cashChange;

    @OneToMany(mappedBy = "shoppingHistory", cascade = CascadeType.ALL)
    private List<ShoppingHistoryItem> shoppingHistoryItems = new ArrayList<>();

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public ShoppingHistory() {
    }

    public ShoppingHistory(String cpf, Double totalInvestedValue, double cashChange, List<ShoppingHistoryItem> shoppingHistoryItems) {
        this.cpf = cpf;
        this.totalInvestedValue = totalInvestedValue;
        this.cashChange = cashChange;
        this.shoppingHistoryItems = shoppingHistoryItems;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Double getTotalInvestedValue() {
        return totalInvestedValue;
    }

    public void setTotalInvestedValue(Double totalInvestedValue) {
        this.totalInvestedValue = totalInvestedValue;
    }

    @JsonManagedReference
    public List<ShoppingHistoryItem> getShoppingHistoryItems() {
        return shoppingHistoryItems;
    }

    public double getCashChange() {
        return cashChange;
    }

    public void setCashChange(double cashChange) {
        this.cashChange = cashChange;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
