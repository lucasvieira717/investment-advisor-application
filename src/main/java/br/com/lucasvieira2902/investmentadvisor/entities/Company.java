package br.com.lucasvieira2902.investmentadvisor.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "companies")
public class Company extends RepresentationModel<Company> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "ind_status", nullable = false, columnDefinition = "boolean default true")
    private Boolean indStatus;

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
    private List<Stock> stocks = new ArrayList<>();

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public Company() {
    }

    public Company(UUID id, String name, Boolean indStatus, List<Stock> stocks) {
        this.id = id;
        this.name = name;
        this.indStatus = indStatus;
        this.stocks = stocks;
    }

    public Company(String name) {
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonManagedReference
    public List<Stock> getStocks() {
        return stocks;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getIndStatus() {
        return indStatus;
    }

    public void setIndStatus(Boolean indStatus) {
        this.indStatus = indStatus;
    }

}
