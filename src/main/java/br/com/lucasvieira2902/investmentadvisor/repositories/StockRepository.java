package br.com.lucasvieira2902.investmentadvisor.repositories;

import br.com.lucasvieira2902.investmentadvisor.entities.Company;
import br.com.lucasvieira2902.investmentadvisor.entities.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface StockRepository extends JpaRepository<Stock, UUID> {
    boolean existsByTicker(String ticker);
    Company findByCompanyId(String id);
    List<Stock> findAllByCompanyIndStatusAndPriceLessThanEqual(boolean companyIndStatus, double totalInvestment);
    Stock findFirstByOrderByPriceAsc();
    Stock findOneById(UUID id);
}
