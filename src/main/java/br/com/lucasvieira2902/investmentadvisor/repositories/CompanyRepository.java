package br.com.lucasvieira2902.investmentadvisor.repositories;

import br.com.lucasvieira2902.investmentadvisor.entities.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface CompanyRepository extends JpaRepository<Company, UUID> {
    boolean existsByName(String name);
    Page<Company> findAllByIndStatus(Pageable pageable, Boolean indStatus);
    Company findByName(String name);
    Company findByNameAndIndStatusIsTrue(String name);
}
