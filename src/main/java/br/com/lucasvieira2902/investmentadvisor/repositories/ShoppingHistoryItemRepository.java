package br.com.lucasvieira2902.investmentadvisor.repositories;

import br.com.lucasvieira2902.investmentadvisor.entities.ShoppingHistoryItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ShoppingHistoryItemRepository extends JpaRepository<ShoppingHistoryItem, UUID> {

}
