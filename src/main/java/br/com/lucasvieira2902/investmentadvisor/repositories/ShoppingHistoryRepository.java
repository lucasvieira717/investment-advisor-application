package br.com.lucasvieira2902.investmentadvisor.repositories;

import br.com.lucasvieira2902.investmentadvisor.entities.ShoppingHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ShoppingHistoryRepository extends JpaRepository<ShoppingHistory, UUID> {
}
