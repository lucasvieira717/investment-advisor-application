package br.com.lucasvieira2902.investmentadvisor.services;

import br.com.lucasvieira2902.investmentadvisor.dtos.ShoppingHistoryBuyDto;
import br.com.lucasvieira2902.investmentadvisor.dtos.ShoppingHistorySimulateDto;
import br.com.lucasvieira2902.investmentadvisor.entities.ShoppingHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface IShoppingHistoryService {
    @Transactional
    Object save(ShoppingHistoryBuyDto shoppingHistoryBuyDto) throws Exception;
    Object simulate(ShoppingHistorySimulateDto shoppingHistory);
    Page<ShoppingHistory> findAllShoppingHistory(Pageable pageable);
    Optional<ShoppingHistory> findById(UUID id);
}
