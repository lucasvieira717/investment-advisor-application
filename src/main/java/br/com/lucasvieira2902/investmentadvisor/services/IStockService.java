package br.com.lucasvieira2902.investmentadvisor.services;

import br.com.lucasvieira2902.investmentadvisor.entities.Company;
import br.com.lucasvieira2902.investmentadvisor.entities.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IStockService {
    Stock save(Stock stock);
    Page<Stock> findAll(Pageable pageable);
    Optional<Stock> findById(UUID id);
    void delete(Stock stock);
    boolean existsByTicker(String ticker);
    Company findByCompanyId(String id);
}
