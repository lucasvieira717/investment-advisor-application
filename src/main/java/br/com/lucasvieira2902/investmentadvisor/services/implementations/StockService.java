package br.com.lucasvieira2902.investmentadvisor.services.implementations;

import br.com.lucasvieira2902.investmentadvisor.entities.Company;
import br.com.lucasvieira2902.investmentadvisor.entities.Stock;
import br.com.lucasvieira2902.investmentadvisor.repositories.StockRepository;
import br.com.lucasvieira2902.investmentadvisor.services.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class StockService implements IStockService {

    @Autowired
    private StockRepository stockRepository;

    @Transactional
    public Stock save(Stock stock) {
        return stockRepository.save(stock);
    }

    public Page<Stock> findAll(Pageable pageable) {
        return stockRepository.findAll(pageable);
    }

    public Optional<Stock> findById(UUID id) {
        return stockRepository.findById(id);
    }

    @Transactional
    public void delete(Stock stock) {
        stockRepository.delete(stock);
    }

    public boolean existsByTicker(String ticker) {
        return stockRepository.existsByTicker(ticker);
    }

    public Company findByCompanyId(String id) {
        return stockRepository.findByCompanyId(id);
    }
}
