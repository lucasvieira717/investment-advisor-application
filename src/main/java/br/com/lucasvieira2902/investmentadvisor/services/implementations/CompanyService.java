package br.com.lucasvieira2902.investmentadvisor.services.implementations;

import br.com.lucasvieira2902.investmentadvisor.entities.Company;
import br.com.lucasvieira2902.investmentadvisor.repositories.CompanyRepository;
import br.com.lucasvieira2902.investmentadvisor.services.ICompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Service
public class CompanyService implements ICompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Transactional
    public Company save(Company company) {
        return companyRepository.save(company);
    }

    public Page<Company> findAll(Pageable pageable) {
        return companyRepository.findAll(pageable);
    }

    public boolean existsByName(String name) {
        return companyRepository.existsByName(name);
    }

    public Optional<Company> findById(UUID id) {
        return companyRepository.findById(id);
    }

    @Transactional
    public void delete(Company company) {
        companyRepository.delete(company);
    }

    public Page<Company> findAllByIndStatus(Pageable pageable, Boolean indStatus) {
        return companyRepository.findAllByIndStatus(pageable, indStatus);
    }

    public Company findByNameAndIndStatusIsTrue(String name) {
        return companyRepository.findByNameAndIndStatusIsTrue(name);
    }
}
