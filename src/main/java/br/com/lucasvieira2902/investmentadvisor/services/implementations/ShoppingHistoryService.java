package br.com.lucasvieira2902.investmentadvisor.services.implementations;

import br.com.lucasvieira2902.investmentadvisor.dtos.*;
import br.com.lucasvieira2902.investmentadvisor.entities.Company;
import br.com.lucasvieira2902.investmentadvisor.entities.ShoppingHistory;
import br.com.lucasvieira2902.investmentadvisor.entities.ShoppingHistoryItem;
import br.com.lucasvieira2902.investmentadvisor.entities.Stock;
import br.com.lucasvieira2902.investmentadvisor.repositories.ShoppingHistoryItemRepository;
import br.com.lucasvieira2902.investmentadvisor.repositories.ShoppingHistoryRepository;
import br.com.lucasvieira2902.investmentadvisor.repositories.StockRepository;
import br.com.lucasvieira2902.investmentadvisor.services.IShoppingHistoryService;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class ShoppingHistoryService  implements IShoppingHistoryService {

    @Autowired
    private ShoppingHistoryRepository shoppingHistoryRepository;

    @Autowired
    private ShoppingHistoryItemRepository shoppingHistoryItemRepository;

    @Autowired
    private StockRepository stockRepository;

    @Transactional
    public Object save(ShoppingHistoryBuyDto shoppingHistoryBuyDto) throws Exception {
        ShoppingHistory shoppingHistory = new ShoppingHistory();
        shoppingHistory.setId(UUID.randomUUID());
        shoppingHistory.setCpf(shoppingHistoryBuyDto.getCpf());
        shoppingHistory.setCashChange(0.0);
        shoppingHistory.setTotalInvestedValue(Precision.round(shoppingHistoryBuyDto.getTotalInvestedValue(), 2));

        double valueTotal = 0.0;
        for (ShoppingHistoryItemBuyDto item : shoppingHistoryBuyDto.getItems()) {
            Stock stock = stockRepository.findOneById(item.getId());
            if (stock == null) {
                throw new Exception("Error: Stock not found");
            } else {
                if(!stock.getCompany().getIndStatus()){
                    throw new Exception("Error: Company " + stock.getCompany().getName() + " of stock " + stock.getTicker() + " is inactive");
                }
                ShoppingHistoryItem shoppingHistoryItem = new ShoppingHistoryItem();
                shoppingHistoryItem.setStock(stock);
                shoppingHistoryItem.setQuantity(item.getQuantity());
                shoppingHistoryItem.setPrice(stock.getPrice());
                shoppingHistoryItem.setShoppingHistory(shoppingHistory);
                shoppingHistory.getShoppingHistoryItems().add(shoppingHistoryItem);
                stock.setQuantity(stock.getQuantity() - item.getQuantity());
                valueTotal += (stock.getPrice() * item.getQuantity());


                if(stock.getQuantity() < 0) {
                    throw new ArithmeticException("Error: The quantity of the stock " + stock.getTicker() + " not available, because it has " +
                            (stock.getQuantity() + item.getQuantity()) + " stocks");
                }

                if(valueTotal > shoppingHistoryBuyDto.getTotalInvestedValue()) {
                    throw new ArithmeticException("Error: Total invested value is greater than total invested value");
                }


                stockRepository.save(stock);
            }
            shoppingHistory.setCashChange(Precision.round(shoppingHistory.getTotalInvestedValue() - valueTotal, 2));
        }

        return shoppingHistoryRepository.save(shoppingHistory);
    }

    public Object simulate(ShoppingHistorySimulateDto shoppingHistorySimulateDto) {
        ShoppingHistory shoppingHistory = new ShoppingHistory();
        shoppingHistory.setTotalInvestedValue(shoppingHistorySimulateDto.getTotalInvestment());
        shoppingHistory.setCpf(shoppingHistorySimulateDto.getCpf());

        List<Stock> stocks = stockRepository.findAllByCompanyIndStatusAndPriceLessThanEqual(true, shoppingHistorySimulateDto.getTotalInvestment());

        if(stocks.isEmpty() || stocks.size() < shoppingHistorySimulateDto.getQtdCompaniesInvestment()) {
            return "Error: There are not enough companies for the number of companies requested";
        }

        Set<Stock> stocksSet = new HashSet<>(stocks);

        Set<Set<Stock>> combinations = combinations(stocksSet, shoppingHistorySimulateDto.getQtdCompaniesInvestment());

        List<ShoppingHistorySimulateResponseDto> shoppingHistories = new ArrayList<>();

        //Monta os pedidos e calcula o valor a ser investido e o troco total
        for (Set<Stock> combination : combinations) {
            ShoppingHistorySimulateResponseDto shoppingHistoryTemp = new ShoppingHistorySimulateResponseDto();
            shoppingHistoryTemp.setCpf(shoppingHistory.getCpf());
            shoppingHistoryTemp.setTotalInvestedValue(shoppingHistory.getTotalInvestedValue());

            double totalValue = 0.00;
            int qtdCompanies = 0;
            for(Stock stock : combination) {
                ShoppingHistoryItemSimulateResponseDto shoppingHistoryItem = new ShoppingHistoryItemSimulateResponseDto();
                shoppingHistoryItem.setStock(stock);
                shoppingHistoryItem.setPrice(stock.getPrice());
                shoppingHistoryItem.setQuantity(1);

                totalValue += stock.getPrice();
                shoppingHistoryTemp.getItems().add(shoppingHistoryItem);
                qtdCompanies++;
                if(qtdCompanies == shoppingHistorySimulateDto.getQtdCompaniesInvestment()) {
                    totalValue = Precision.round(totalValue, 2);
                    int qtd = (int) Math.floor(shoppingHistorySimulateDto.getTotalInvestment() / totalValue);
                    shoppingHistoryTemp.setTotalInvestedValue(totalValue * qtd);
                    for (ShoppingHistoryItemSimulateResponseDto shoppingHistoryItemTemp : shoppingHistoryTemp.getItems()) {
                        if(shoppingHistoryItemTemp.getStock().getQuantity() > qtd) {
                            shoppingHistoryItemTemp.setQuantity(qtd);
                        }else{
                            shoppingHistoryItemTemp.setQuantity(shoppingHistoryItemTemp.getStock().getQuantity());
                            shoppingHistoryTemp.setTotalInvestedValue(shoppingHistoryTemp.getTotalInvestedValue() - (shoppingHistoryItemTemp.getPrice() * (qtd - shoppingHistoryItemTemp.getQuantity())));
                        }
                        if((shoppingHistoryTemp.getTotalInvestedValue() + shoppingHistoryItemTemp.getPrice()) <= shoppingHistorySimulateDto.getTotalInvestment() &&
                                shoppingHistoryItemTemp.getStock().getQuantity() > qtd) {
                            int qtdTemp = (int) Math.floor((shoppingHistorySimulateDto.getTotalInvestment() - shoppingHistoryTemp.getTotalInvestedValue()) / shoppingHistoryItemTemp.getPrice());
                            shoppingHistoryTemp.setTotalInvestedValue(Precision.round(shoppingHistoryTemp.getTotalInvestedValue() + (shoppingHistoryItemTemp.getPrice() * qtdTemp), 2));
                            shoppingHistoryItemTemp.setQuantity(shoppingHistoryItemTemp.getQuantity() + qtdTemp);
                        }
                    }
                }
            }

            if(totalValue <= shoppingHistorySimulateDto.getTotalInvestment()) {
                shoppingHistoryTemp.setCashChange(Precision.round(shoppingHistorySimulateDto.getTotalInvestment() - shoppingHistoryTemp.getTotalInvestedValue(), 2));
                shoppingHistories.add(shoppingHistoryTemp);
            }
        }

        ShoppingHistorySimulateResponseDto shoppingHistoryResult = null;
        for (ShoppingHistorySimulateResponseDto shoppingHistoryTemp : shoppingHistories) {
            if(shoppingHistoryResult == null) {
                shoppingHistoryResult = shoppingHistoryTemp;
            }else{
                if(shoppingHistoryTemp.getCashChange() == shoppingHistoryResult.getCashChange()) {
                    if(shoppingHistorySimulateDto.getTotalInvestment() / shoppingHistoryResult.getTotalInvestedValue() >
                        shoppingHistoryTemp.getTotalInvestedValue() / shoppingHistoryTemp.getTotalInvestedValue()) {
                        shoppingHistoryResult = shoppingHistoryTemp;
                    }else if(shoppingHistoryTemp.getCashChange() < shoppingHistoryResult.getCashChange()){
                        shoppingHistoryResult = shoppingHistoryTemp;
                    }
                }
            }
        }
;
        return shoppingHistoryResult;
    }

    @Override
    public Page<ShoppingHistory> findAllShoppingHistory(Pageable pageable) {
        return shoppingHistoryRepository.findAll(pageable);
    }

    public Optional<ShoppingHistory> findById(UUID id) {
        return shoppingHistoryRepository.findById(id);
    }

    public static <Stock> Set<Set<Stock>> combinations(Set<Stock> stocksSet, int k){
        if (stocksSet.size() == 0) {
            throw new IllegalArgumentException("stocksSet.size must be > 0");
        }
        if (k < 0) {
            throw new IllegalArgumentException("k must be >= 0");
        }
        if (k == 0) {
            Set<Set<Stock>> result = new HashSet<Set<Stock>>();
            result.add(new HashSet<Stock>());
            return result;
        } else {
            Set<Set<Stock>> result = new HashSet<Set<Stock>>();
            for (Stock e : stocksSet) {
                Set<Stock> newAlphabet = new HashSet<Stock>(stocksSet);
                newAlphabet.remove(e);
                Set<Set<Stock>> smallerCombinations = combinations(newAlphabet, k - 1);
                for (Set<Stock> smallerCombination : smallerCombinations) {
                    Set<Stock> combination = new HashSet<Stock>(smallerCombination);
                    combination.add(e);
                    result.add(combination);
                }
            }

            return result;
        }
    }
}
