package br.com.lucasvieira2902.investmentadvisor.services;

import br.com.lucasvieira2902.investmentadvisor.entities.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.UUID;

public interface ICompanyService {
    Company save(Company company);
    Page<Company> findAll(Pageable pageable);
    boolean existsByName(String name);
    Optional<Company> findById(UUID id);
    void delete(Company company);
    Page<Company> findAllByIndStatus(Pageable pageable, Boolean indStatus);
    Company findByNameAndIndStatusIsTrue(String name);
}
